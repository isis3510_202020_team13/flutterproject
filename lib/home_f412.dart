import 'package:f412/src/Disorders/disorders.dart';
import 'package:f412/src/Doctors/doctorsFB.dart';
import 'package:f412/src/Hospitals/hospitalsFB.dart';
import 'package:f412/src/Medications/medications_fb.dart';
import 'package:f412/src/Pastillero/alarm_page.dart';
import 'package:f412/src/Scheduler/SchedulerFirebase/scheduler_main.dart';
import 'package:f412/src/Scheduler/ShedulerLocal/sheduler_local_main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'src/Scheduler/ui/screens/scheduler.dart';

class HomeF412 extends StatelessWidget {
  String pathImage = "assets/img/prof.jpeg";
  SharedPreferences prefs;
  String name;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget yesButton = FlatButton(
        onPressed: () async => {await launch("tel:+57106")},
        child: Text("Yes"));

    Widget noButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Notice"),
      content: Text("are you sure you want to make the call?"),
      actions: [yesButton, noButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    name = prefs.getString('email') ?? "User";
    print(name);
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //initPrefs();

    // TODO: implement build

    final photo = Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.only(top: 50.0, left: 10.0, right: 20),
        width: 50.0,
        height: 50.0,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.cover, image: AssetImage(pathImage)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black38,
                  blurRadius: 15.0,
                  offset: Offset(0.0, 7.0))
            ]),
      ),
    );

    final hola = Container(
        margin: EdgeInsets.only(top: 60, left: 20.0),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Text(
            "Hello.",
            style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
          ),
        ));

    final nombre = Container(
        margin: EdgeInsets.only(
          top: 0,
          left: 20,
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Text(
            _auth.currentUser.displayName + "!!",
            style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
          ),
        ));

    final banner = Material(
      child: InkWell(
        child: Container(
            height: 150.0,
            width: width,
            margin: EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                shape: BoxShape.rectangle,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black38,
                      blurRadius: 15.0,
                      offset: Offset(0.0, 7.0))
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 20, top: 35.0),
                  height: 150,
                  width: 200,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        "Your mental health is very important.",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                      Expanded(
                          child: Text(
                        "If you have a psichriatic emergency click here.",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                            color: Colors.white),
                      ))
                    ],
                  ),
                  alignment: Alignment.topLeft,
                ),
                Container(
                  child: Expanded(
                      child: Image(
                          image: AssetImage("assets/img/bannerimagedef.png"))),
                ),
              ],
            )),
        onTap: () {
          showAlertDialog(context);
        },
      ),
    );

    final pastillero = Column(
      children: <Widget>[
        Material(
          child: InkWell(
            child: Container(
                margin: EdgeInsets.only(top: 20),
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0))
                    ]),
                child: Icon(FontAwesomeIcons.pills,
                    color: Colors.deepOrangeAccent, size: 30.0)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return AlarmPage();
              }));
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Pillbox",
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        )
      ],
    );

    final scheduler = Column(
      children: <Widget>[
        Material(
          child: InkWell(
            child: Container(
                margin: EdgeInsets.only(top: 20),
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0))
                    ]),
                child: Icon(FontAwesomeIcons.calendarAlt,
                    color: Colors.deepOrangeAccent, size: 30.0)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return SchedulerLocalHomePage();
              }));
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Scheduler",
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        )
      ],
    );
    final disorders = Column(
      children: <Widget>[
        Material(
          child: InkWell(
            child: Container(
                margin: EdgeInsets.only(top: 20),
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0))
                    ]),
                child: Icon(FontAwesomeIcons.brain,
                    color: Colors.deepOrangeAccent, size: 30.0)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return DisordersFB();
              }));
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Disorders",
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        )
      ],
    );

    final medications = Column(
      children: <Widget>[
        Material(
          child: InkWell(
            child: Container(
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0))
                    ]),
                child: Icon(FontAwesomeIcons.fileMedical,
                    color: Colors.deepOrangeAccent, size: 30.0)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return MedicationsFB();
              }));
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Medications",
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        )
      ],
    );

    final hospitals = Column(
      children: <Widget>[
        Material(
          child: InkWell(
            child: Container(
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0))
                    ]),
                child: Icon(FontAwesomeIcons.hospital,
                    color: Colors.deepOrangeAccent, size: 30.0)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return HospitalsFB();
              }));
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Hospitals",
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        )
      ],
    );

    final doctors = Column(
      children: <Widget>[
        Material(
          child: InkWell(
            child: Container(
                height: 100.0,
                width: 100.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0))
                    ]),
                child: Icon(FontAwesomeIcons.userMd,
                    color: Colors.deepOrangeAccent, size: 30.0)),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return DoctorsFB();
              }));
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Doctors",
            style: TextStyle(fontWeight: FontWeight.normal),
          ),
        )
      ],
    );

    final servicios1 = Container(
      width: width,
      margin: EdgeInsets.only(top: 0, left: 20, right: 20),
      child: Row(
        children: <Widget>[
          Expanded(child: FittedBox(fit: BoxFit.scaleDown, child: pastillero)),
          Expanded(child: FittedBox(fit: BoxFit.scaleDown, child: scheduler)),
          Expanded(child: FittedBox(fit: BoxFit.scaleDown, child: disorders)),
          //Flexible(child: pastillero, fit: FlexFit.tight),
          //Flexible(child: diario, fit: FlexFit.tight),
          // Flexible(child: trastornos, fit: FlexFit.tight),
        ],
      ),
    );

    final servicios2 = Container(
      width: width,
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        children: <Widget>[
          Expanded(child: FittedBox(fit: BoxFit.scaleDown, child: medications)),
          Expanded(child: FittedBox(fit: BoxFit.scaleDown, child: hospitals)),
          Expanded(child: FittedBox(fit: BoxFit.scaleDown, child: doctors)),
          //Flexible(child: farmacos, fit: FlexFit.tight)
          //Flexible(child: clinicas, fit: FlexFit.tight),
          //Flexible(child: doctores, fit: FlexFit.tight),
        ],
      ),
    );

    final servicios = Container(
      margin: EdgeInsets.only(
        top: 30,
        left: 20,
      ),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Text(
          "Services:",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0),
        ),
      ),
    );

    final upPart = Container(
      child: Column(
        children: <Widget>[photo, hola, nombre, banner],
      ),
    );

    final downPart = Container(
      width: width,
      child: Column(
        children: <Widget>[servicios, servicios1, servicios2],
      ),
    );

    return Stack(
      children: <Widget>[
        ListView(
          children: <Widget>[upPart, downPart],
        )
      ],
    );
  }
}
