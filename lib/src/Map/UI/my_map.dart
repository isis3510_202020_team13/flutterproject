import 'package:f412/src/Map/Model/pin_data.dart';
import 'package:f412/src/Map/Util/theme.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:url_launcher/url_launcher.dart';

class MyMap extends StatefulWidget {
  String name;
  double latitude;
  double longitude;
  MyMap(this.name, this.latitude, this.longitude);
  @override
  _MyMapState createState() =>
      _MyMapState(this.name, this.latitude, this.longitude);
}

class _MyMapState extends State<MyMap> {
  String name;
  double latitude;
  double longitude;

  _MyMapState(this.name, this.latitude, this.longitude);

  GoogleMapController _controller;
  Position position;

  Widget _child = Center(
    child: Text('Loading...'),
  );
  BitmapDescriptor _sourceIcon;

  double _pinPillPosition = -100;

  PinData _currentPinData() {
    return PinData(
        pinPath: 'assets/img/pin.png',
        avatarPath: 'assets/img/h8.jpg',
        location: LatLng(latitude, longitude),
        locationName: name,
        labelColor: Colors.grey);
  }

  PinData _sourcePinInfo;

  void _setSourceIcon() async {
    _sourceIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), 'assets/img/pin.png');
  }

  Future<void> getPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.location);

    if (permission == PermissionStatus.denied) {
      await PermissionHandler()
          .requestPermissions([PermissionGroup.locationAlways]);
    }

    var geolocator = Geolocator();

    GeolocationStatus geolocationStatus =
        await geolocator.checkGeolocationPermissionStatus();

    switch (geolocationStatus) {
      case GeolocationStatus.denied:
        showToast('Access denied');
        break;
      case GeolocationStatus.disabled:
        showToast('Disabled');
        break;
      case GeolocationStatus.restricted:
        showToast('restricted');
        break;
      case GeolocationStatus.unknown:
        showToast('Unknown');
        break;
      case GeolocationStatus.granted:
        _getCurrentLocation();
    }
  }

  void _getCurrentLocation() async {
    Position res = await Geolocator().getCurrentPosition();
    setState(() {
      position = res;
      _child = _mapWidget();
    });
  }

  void _setStyle(GoogleMapController controller) async {
    String value = await DefaultAssetBundle.of(context)
        .loadString('assets/map_style.json');

    controller.setMapStyle(value);
  }

  Set<Marker> _createMarker() {
    return <Marker>[
      Marker(
          markerId: MarkerId('home'),
          position: LatLng(_currentPinData().location.latitude,
              _currentPinData().location.longitude),
          icon: _sourceIcon,
          onTap: () {
            setState(() {
              _pinPillPosition = 0;
            });
          })
    ].toSet();
  }

  void showToast(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  void initState() {
    getPermission();
    _setSourceIcon();
    super.initState();
  }

  Widget _mapWidget() {
    return GoogleMap(
      zoomControlsEnabled: false,
      mapType: MapType.normal,
      markers: _createMarker(),
      initialCameraPosition: CameraPosition(
          target: LatLng(_currentPinData().location.latitude,
              _currentPinData().location.longitude),
          zoom: 18.0),
      onMapCreated: (GoogleMapController controller) {
        _controller = controller;
        _setStyle(controller);
        _pinPillPosition = -100;
      },
      tiltGesturesEnabled: false,
      onTap: (LatLng location) {
        setState(() {
          _pinPillPosition = -100;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        title: Text(
          "Map Navigator",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          _child,
          Container(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.all(20),
                height: 70,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        blurRadius: 20,
                        offset: Offset.zero,
                        color: Colors.grey.withOpacity(0.5),
                      )
                    ]),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _buildAvatar(),
                    _buildLocationInfo(),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.white,
          label: Text(
            "get directions",
            style: TextStyle(color: Colors.lightBlue),
          ),
          icon: Icon(
            FontAwesomeIcons.mapMarkerAlt,
            color: Colors.lightBlue,
          ),
          onPressed: () async {
            Position res = await Geolocator().getCurrentPosition();
            double sourceLatitude = res.latitude;
            double sourceLongitude = res.longitude;
            double destinationLatitude = _currentPinData().location.latitude;
            double destinationLongitude = _currentPinData().location.longitude;
            launchMapsUrl(sourceLatitude, sourceLongitude, destinationLatitude,
                destinationLongitude);
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  Widget _buildAvatar() {
    return Container(
      margin: EdgeInsets.only(left: 10),
      width: 50,
      height: 50,
      child: ClipOval(
        child: Image.asset(
          _currentPinData().avatarPath,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildLocationInfo() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(left: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              _currentPinData().locationName,
              // ignore: deprecated_member_use
              style: CustomAppTheme().data.textTheme.subtitle,
            ),
            Text(
              'Latitude : ${_currentPinData().location.latitude}',
              // ignore: deprecated_member_use
              style: CustomAppTheme().data.textTheme.display1,
            ),
            Text(
              'Longitude : ${_currentPinData().location.longitude}',
              // ignore: deprecated_member_use
              style: CustomAppTheme().data.textTheme.display1,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildMarkerType() {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Image.asset(
        _currentPinData().pinPath,
        width: 50,
        height: 50,
      ),
    );
  }

  static void launchMapsUrl(sourceLatitude, sourceLongitude,
      destinationLatitude, destinationLongitude) async {
    String mapOptions = [
      'saddr=$sourceLatitude,$sourceLongitude',
      'daddr=$destinationLatitude,$destinationLongitude',
      'dir_action=navigate'
    ].join('&');

    final url = 'https://www.google.com/maps?$mapOptions';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
