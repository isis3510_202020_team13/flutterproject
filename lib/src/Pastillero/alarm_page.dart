import 'package:dotted_border/dotted_border.dart';
import 'package:f412/main.dart';
import 'package:f412/src/Pastillero/pillbox_theme_data.dart';
import 'package:f412/src/Provider/entry_provider.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'alarm_helper.dart';
import 'alarm_info.dart';
import 'package:intl/intl.dart';

class AlarmPage extends StatefulWidget {
  @override
  _AlarmPageState createState() => _AlarmPageState();
}

class _AlarmPageState extends State<AlarmPage> {
  final FirebaseAnalytics _firebaseAnalytics = FirebaseAnalytics();
  SharedPreferences prefs;
  DateTime _alarmTime;
  String _alarmTimeString;
  String _alarmTitle;
  AlarmHelper _alarmHelper = AlarmHelper();
  Future<List<AlarmInfo>> _alarms;
  List<AlarmInfo> _currentAlarms;
  EntryProvider entryProvider;

  @override
  void initState() {
    _alarmTime = DateTime.now();
    entryProvider = EntryProvider();
    _alarmHelper.initializeDatabase().then((value) {
      print('------database intialized');
      loadAlarms();
      initPrefs();
    });
    super.initState();
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  void loadAlarms() {
    _alarms = _alarmHelper.getAlarms();
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[],
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.lightBlue,
          ),
          title: Text(
            "Pillbox",
            style: TextStyle(
              color: Colors.lightBlue,
            ),
          ),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: FutureBuilder<List<AlarmInfo>>(
                  future: _alarms,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      _currentAlarms = snapshot.data;
                      return ListView(
                        children: snapshot.data.map<Widget>((alarm) {
                          var alarmTime = DateFormat('hh:mm aa')
                              .format(alarm.alarmDateTime);
                          var gradientColor = GradientTemplate
                              .gradientTemplate[alarm.gradientColorIndex]
                              .colors;
                          return Container(
                            margin: const EdgeInsets.only(bottom: 32),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: gradientColor,
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: gradientColor.last.withOpacity(0.4),
                                  blurRadius: 8,
                                  spreadRadius: 2,
                                  offset: Offset(4, 4),
                                ),
                              ],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(24)),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          FontAwesomeIcons.pills,
                                          color: Colors.white,
                                          size: 24,
                                        ),
                                        SizedBox(width: 8),
                                        Text(
                                          "Pillbox Reminder",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                              fontFamily: 'avenir'),
                                        ),
                                      ],
                                    ),
                                    Switch(
                                      onChanged: (bool value) {},
                                      value: true,
                                      activeColor: Colors.white,
                                    ),
                                  ],
                                ),
                                Text(
                                  'Mon-Fri',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'avenir'),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      alarmTime,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'avenir',
                                          fontSize: 24,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.delete),
                                      color: Colors.white,
                                      onPressed: () {
                                        deleteAlarm(alarm.id);
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        }).followedBy([
                          if (_currentAlarms.length < 5)
                            DottedBorder(
                              strokeWidth: 2,
                              color: CustomColors.clockOutline,
                              borderType: BorderType.RRect,
                              radius: Radius.circular(24),
                              dashPattern: [5, 4],
                              child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(24)),
                                ),
                                child: FlatButton(
                                  onPressed: () {
                                    _alarmTimeString = DateFormat('HH:mm')
                                        .format(DateTime.now());

                                    showModalBottomSheet(
                                      isScrollControlled: true,
                                      useRootNavigator: true,
                                      context: context,
                                      clipBehavior: Clip.antiAliasWithSaveLayer,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(24),
                                        ),
                                      ),
                                      builder: (context) {
                                        return AnimatedPadding(
                                          padding:
                                              MediaQuery.of(context).viewInsets,
                                          duration:
                                              const Duration(milliseconds: 100),
                                          curve: Curves.decelerate,
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.2,
                                            child: StatefulBuilder(
                                              builder:
                                                  (context, setModalState) {
                                                TextEditingController cont =
                                                    new TextEditingController();
                                                _alarmTitle = cont.text;
                                                return SingleChildScrollView(
                                                  child: Column(
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                          Icon(FontAwesomeIcons
                                                              .clock),
                                                          FlatButton(
                                                            onPressed:
                                                                () async {
                                                              var selectedTime =
                                                                  await showTimePicker(
                                                                context:
                                                                    context,
                                                                initialTime:
                                                                    TimeOfDay
                                                                        .now(),
                                                              );
                                                              if (selectedTime !=
                                                                  null) {
                                                                final now =
                                                                    DateTime
                                                                        .now();

                                                                var selectedDateTime = DateTime(
                                                                    now.year,
                                                                    now.month,
                                                                    now.day,
                                                                    selectedTime
                                                                        .hour,
                                                                    selectedTime
                                                                        .minute);
                                                                _alarmTime =
                                                                    selectedDateTime;
                                                                setModalState(
                                                                    () {
                                                                  _alarmTimeString =
                                                                      DateFormat(
                                                                              'HH:mm')
                                                                          .format(
                                                                              selectedDateTime);
                                                                });
                                                              }
                                                            },
                                                            child: Text(
                                                              _alarmTimeString,
                                                              style: TextStyle(
                                                                  fontSize: 32),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        height: 40,
                                                        margin:
                                                            EdgeInsets.all(10),
                                                        child: TextFormField(
                                                          controller: cont,
                                                          decoration:
                                                              InputDecoration(
                                                            labelText: "title",
                                                            border:
                                                                OutlineInputBorder(),
                                                          ),
                                                        ),
                                                      ),
                                                      FloatingActionButton
                                                          .extended(
                                                        onPressed: onSaveAlarm,
                                                        backgroundColor:
                                                            Colors.lightBlue,
                                                        label: Text('Save'),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              },
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                    // scheduleAlarm();
                                  },
                                  child: Column(
                                    children: [
                                      Image.asset(
                                        'assets/img/add_alarm.png',
                                        scale: 1.5,
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        'Add new Reminder',
                                        style:
                                            TextStyle(color: Colors.lightBlue),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          else
                            Center(
                                child: Text(
                                    'For reducing the risk of over medication we limited to 5 the amount of Medications reminders, please errase older reminders.')),
                        ]).toList(),
                      );
                    }

                    return Center(
                      child: Text(
                        'Loading..',
                        style: TextStyle(color: Colors.white),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ));
  }

  void scheduleAlarm(
      DateTime scheduleNotificationDateTime, AlarmInfo alarmInfo) async {
    var scheduleNotificationDateTime = DateTime.now().add(Duration(seconds: 5));

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'alarm_notif',
      'alarm_notif',
      'Channel for Alarm notification',
      icon: '@mipmap/ic_launcher',
      sound: RawResourceAndroidNotificationSound('a_long_cold_sting'),
      largeIcon: DrawableResourceAndroidBitmap('@mipmap/ic_launcher'),
    );

    var iOSPlatformChannelSpecifics = IOSNotificationDetails(
        sound: 'a_long_sold_sting.wav',
        presentAlert: true,
        presentBadge: true,
        presentSound: true);
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        0,
        "F412 Pillbox Reminder",
        "Hey! Don't forget to take: 100Mg Lorazepam",
        alarmInfo.alarmDateTime,
        platformChannelSpecifics);
  }

  Future saveLog() async {
    String emailPref = prefs.getString('email') ?? " ";
    entryProvider.changeEntry = emailPref;
    entryProvider.changeDate = new DateTime.now();
    entryProvider.saveEntryAlarm();
    //print('+++++++++++++++++++++++++++++++++++++++++' + entryProvider.entry);
    //print('+++++++++++++++++++++++++++++++++++++++++' +
    //    entryProvider.date.toString());
  }

  Future getPosts() async {
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot qn = await firestore.collection("Disorders").get();

    return qn.docs;
  }

  void onSaveAlarm() async {
    DateTime scheduleAlarmDateTime;
    if (_alarmTime.isAfter(DateTime.now()))
      scheduleAlarmDateTime = _alarmTime;
    else
      scheduleAlarmDateTime = _alarmTime.add(Duration(days: 1));

    var alarmInfo = AlarmInfo(
      alarmDateTime: scheduleAlarmDateTime,
      gradientColorIndex: _currentAlarms.length,
      title: _alarmTitle,
    );
    _alarmHelper.insertAlarm(alarmInfo);
    scheduleAlarm(scheduleAlarmDateTime, alarmInfo);
    Navigator.pop(context);
    loadAlarms();
    await saveLog();
  }

  void deleteAlarm(int id) {
    _alarmHelper.delete(id);
    //unsubscribe for notification
    loadAlarms();
  }
}
