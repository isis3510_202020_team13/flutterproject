import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DisordersFB extends StatefulWidget {
  @override
  _DisordersFBState createState() => _DisordersFBState();
}

class _DisordersFBState extends State<DisordersFB> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget> [

        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        title: Text(
          "Disorders",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: ListPage(),

    );
  }
}
class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {

  Future getPosts() async {
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot qn = await firestore.collection("Disorders").get();

    return qn.docs;
  }

  navigateToDetail(DocumentSnapshot disorder){
    Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(disorders: disorder,)));
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference disorders = FirebaseFirestore.instance.collection("Disorders");

    return StreamBuilder<QuerySnapshot>(
      stream: disorders.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
        if(snapshot.hasError){
          return Center( child: Text("Something went wrong"),);
        }
        if(snapshot.connectionState == ConnectionState.waiting){
          return Text("Loading");
        }
        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document){
            return new ListTile(
              leading: Icon(FontAwesomeIcons.pills, color: Colors.lightBlue,),
              title: Text(
                document.data()['tittle'],
              ),
              onTap: () => navigateToDetail(document),
            );
          }).toList(),
        );
      },
    );
  }
}

class DetailPage extends StatefulWidget {
  final DocumentSnapshot disorders;
  DetailPage({this.disorders});
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget> [

        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        title: Text(
          "",
          style: TextStyle(
            color: Colors.blueAccent,
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
            right: 10.0,
            bottom: 10.0
        ),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                widget.disorders.data()['tittle'],
                style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.lightBlue
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                widget.disorders.data()['description'],
                style: TextStyle(
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                "Symptoms",
                style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.lightBlue
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                widget.disorders.data()['symptoms'],
                style: TextStyle(
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

