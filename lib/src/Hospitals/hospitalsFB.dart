import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:f412/src/Hospitals/hospital_card.dart';
import 'package:f412/src/Map/UI/my_map.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';

class HospitalsFB extends StatefulWidget {
  @override
  _HospitalsFBState createState() => _HospitalsFBState();
}

class _HospitalsFBState extends State<HospitalsFB> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget> [

        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        title: Text(
          "Hospitals",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: HospitalsListPage(),
    );
  }
}

class HospitalsListPage extends StatefulWidget {
  @override
  _HospitalsListPageState createState() => _HospitalsListPageState();
}

class _HospitalsListPageState extends State<HospitalsListPage> {


  Future getPosts() async {
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot qn = await firestore.collection("Hospitals").get();

    return qn.docs;
  }
  navigateToDetail(DocumentSnapshot hospital){
    Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(hospitals: hospital,)));
  }


  @override
  Widget build(BuildContext context) {

    CollectionReference hospitals = FirebaseFirestore.instance.collection("Hospitals");
    FirebaseStorage storage = FirebaseStorage.instance;

    return StreamBuilder<QuerySnapshot>(
          stream: hospitals.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if(snapshot.hasError){
              return Center(child: Text("Something went wrong"),);
            }
            if(snapshot.connectionState == ConnectionState.waiting){
              return Text("Loading");
            }
            return new ListView(
              children: snapshot.data.docs.map((DocumentSnapshot document){
                var name = document.data()['name'];
                var address = document.data()['address'];
                var rating = document.data()['rating'].toDouble();
                var pathImage = document.data()['img'];
                var email = document.data()['email'];
                NetworkImage img = NetworkImage(pathImage);
                var phoneNumber = document.data()['phone'];

                return InkWell(
                    child: new HospitalsCard(name, address, rating, pathImage, email, phoneNumber),
                  onTap: () => navigateToDetail(document),
                );
              }).toList(),
            );
      },
    );
  }
}

class DetailPage extends StatefulWidget {
  final DocumentSnapshot hospitals;
  DetailPage({this.hospitals});
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {

  @override
  Widget build(BuildContext context) {
    return MyMap(widget.hospitals.data()['name'],widget.hospitals.data()['latitude'], widget.hospitals.data()['longitude']);
  }
}


