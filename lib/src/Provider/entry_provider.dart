import 'package:f412/src/Entries/entryEventAlarm.dart';
import 'package:f412/src/Entries/entryEventSchedule.dart';
import 'package:f412/src/Services/firestore_service.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class EntryProvider with ChangeNotifier {
  final firestoreService = FirestoreService();
  DateTime _date;
  String _email;

  String _entryId;
  var uuid = Uuid();

  //Getters
  DateTime get date => _date;
  String get entry => _email;
  //Stream<List<Entry>> get entries => firestoreService.getEntries();

  //Setters
  set changeDate(DateTime date) {
    _date = date;
    notifyListeners();
  }

  set changeEntry(String entry) {
    _email = entry;
    notifyListeners();
  }

  //Functions
  saveEntry() {
    if (_entryId == null) {
      //Add
      var newEntry = EntrySchedule(
          date: _date.toIso8601String(), email: _email, entryId: uuid.v1());
      print(newEntry.email);
      firestoreService.setEntrySchedule(newEntry);
    } else {
      //Edit
      var updatedEntry = EntrySchedule(
          date: _date.toIso8601String(), email: _email, entryId: _entryId);
      firestoreService.setEntrySchedule(updatedEntry);
    }
  }

  saveEntryAlarm() {
    if (_entryId == null) {
      //Add
      var newEntry = EntryAlarm(
          date: _date.toIso8601String(), mail: _email, entryId: uuid.v1());
      print(newEntry.mail);
      firestoreService.setEntryAlarm(newEntry);
    } else {
      //Edit
      var updatedEntry = EntryAlarm(
          date: _date.toIso8601String(), mail: _email, entryId: _entryId);
      firestoreService.setEntryAlarm(updatedEntry);
    }
  }

  removeEntry(String entryId) {
    firestoreService.removeEntrySchedule(entryId);
  }
}
