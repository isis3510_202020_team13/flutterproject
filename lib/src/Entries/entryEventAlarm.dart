class EntryAlarm {
  String date;
  String mail;
  String entryId;

  EntryAlarm({this.entryId, this.date, this.mail});

  factory EntryAlarm.fromJson(Map<String, dynamic> parsedJson) {
    return EntryAlarm(
        entryId: parsedJson['entryId'],
        date: parsedJson['date'],
        mail: parsedJson['mail']);
  }

  Map<String, dynamic> toMap() {
    return {'entryId': entryId, 'date': date, 'mail': mail};
  }
}
