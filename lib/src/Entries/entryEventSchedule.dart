class EntrySchedule {
  String date;
  String email;
  String entryId;

  EntrySchedule({this.entryId, this.date, this.email});

  factory EntrySchedule.fromJson(Map<String, dynamic> parsedJson) {
    return EntrySchedule(
        entryId: parsedJson['entryId'],
        date: parsedJson['date'],
        email: parsedJson['email']);
  }

  Map<String, dynamic> toMap() {
    return {'entryId': entryId, 'date': date, 'email': email};
  }
}
