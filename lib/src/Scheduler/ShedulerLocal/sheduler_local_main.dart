import 'dart:convert';
import 'dart:ui';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:f412/src/Provider/entry_provider.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';

class SchedulerLocalHomePage extends StatefulWidget {
  @override
  _SchedulerLocalHomePageState createState() => _SchedulerLocalHomePageState();
}

class _SchedulerLocalHomePageState extends State<SchedulerLocalHomePage> {
  final FirebaseAnalytics _firebaseAnalytics = FirebaseAnalytics();
  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _firebaseAnalytics);
  CalendarController _controller;
  Map<DateTime, List<dynamic>> _events;
  List<dynamic> _selectedEvents;
  TextEditingController _eventController;
  TextEditingController _startController;
  TextEditingController _endController;
  TextEditingController prioritiesSelected;
  String selectPriority;
  SharedPreferences prefs;
  List<String> priorities = ["High", "Medium", "Low"];
  EntryProvider entryProvider;
  DateTime finalTime;

  @override
  void initState() {
    super.initState();
    _controller = CalendarController();
    _eventController = TextEditingController();
    _startController = TextEditingController();
    _endController = TextEditingController();
    prioritiesSelected = TextEditingController();
    selectPriority = "";
    _events = {};
    _selectedEvents = [];
    entryProvider = EntryProvider();
    initPrefs();
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      _events = Map<DateTime, List<dynamic>>.from(
          decodeMap(json.decode(prefs.getString("events") ?? "{}")));
      print(_events);
    });
  }

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.deepOrangeAccent,
        ),
        title: Text(
          'Scheduler',
          style: TextStyle(color: Colors.deepOrangeAccent),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: 10,
                left: 15,
                right: 15,
              ),
              child: Card(
                child: TableCalendar(
                  events: _events,
                  initialCalendarFormat: CalendarFormat.week,
                  calendarStyle: CalendarStyle(
                      canEventMarkersOverflow: false,
                      todayColor: Colors.orange,
                      selectedColor: Colors.orange[200],
                      todayStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                          color: Colors.white)),
                  headerStyle: HeaderStyle(
                    centerHeaderTitle: true,
                    formatButtonDecoration: BoxDecoration(
                      color: Colors.orange,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    formatButtonTextStyle: TextStyle(color: Colors.white),
                    formatButtonShowsNext: false,
                  ),
                  startingDayOfWeek: StartingDayOfWeek.monday,
                  onDaySelected: (date, events, _selected) {
                    setState(() {
                      events
                          .sort((a, b) => a.toString().compareTo(b.toString()));
                      _selectedEvents = events;
                    });
                  },
                  builders: CalendarBuilders(
                    selectedDayBuilder: (context, date, events) => Container(
                        margin: const EdgeInsets.all(4.0),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.orange[200],
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Text(
                          date.day.toString(),
                          style: TextStyle(color: Colors.white),
                        )),
                    todayDayBuilder: (context, date, events) => Container(
                        margin: const EdgeInsets.all(4.0),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Text(
                          date.day.toString(),
                          style: TextStyle(color: Colors.white),
                        )),
                  ),
                  calendarController: _controller,
                ),
              ),
            ),
            ..._selectedEvents.map((event) => Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    color: Colors.blue[100],
                    child: Container(
                      margin: EdgeInsets.all(10),
                      child: Text(
                        event,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ),
                )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.deepOrangeAccent,
        child: Icon(Icons.add),
        onPressed: _showAddDialog,
      ),
    );
  }

  String formatTimeOfDay(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    final format = DateFormat.jm(); //"6:00 AM"
    //_firebaseAnalytics.logEvent(name: null);
    return format.format(dt);
  }

  Future saveLog() async {
    String emailPref = prefs.getString('email') ?? " ";
    entryProvider.changeEntry = emailPref;
    entryProvider.changeDate = new DateTime.now();
    entryProvider.saveEntry();
  }

  _showAddDialog() async {
    await showDialog(
        barrierDismissible: false,
        useRootNavigator: true,
        context: context,
        builder: (context) => AlertDialog(
              title: Text("Add Event"),
              scrollable: true,
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Container(
                          child: TextFormField(
                            controller: _startController, // add this line.
                            decoration: InputDecoration(
                                labelText: 'Starts At:',
                                border: OutlineInputBorder()),
                            onTap: () async {
                              TimeOfDay time = TimeOfDay.now();
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());

                              TimeOfDay picked = await showTimePicker(
                                  context: context, initialTime: time);
                              if (picked != null && picked != time) {
                                _startController.text =
                                    formatTimeOfDay(picked); // add this line.
                                setState(() {
                                  time = picked;
                                });
                              }
                            },
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'cant be empty';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 10,
                          ),
                          child: TextFormField(
                            controller: _endController, // add this line.
                            decoration: InputDecoration(
                              labelText: 'Ends At:',
                              border: OutlineInputBorder(),
                            ),
                            onTap: () async {
                              TimeOfDay time = TimeOfDay.now();
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());

                              TimeOfDay picked = await showTimePicker(
                                  context: context, initialTime: time);
                              if (picked != null && picked != time) {
                                _endController.text =
                                    formatTimeOfDay(picked); // add this line.
                                setState(() {
                                  time = picked;
                                });
                              }
                            },
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'cant be empty';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: TextFormField(
                      keyboardType: TextInputType.name,
                      controller: _eventController,
                      decoration: InputDecoration(
                          labelText: "Description",
                          border: OutlineInputBorder()),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'cant be empty';
                        }
                        return null;
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: DropDownField(
                      controller: prioritiesSelected,
                      hintText: "Select the priority of the event",
                      required: true,
                      enabled: true,
                      items: priorities,
                      onValueChanged: (value) {
                        setState(() {
                          selectPriority = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Cancel"),
                ),
                FlatButton(
                  child: Text("Save"),
                  onPressed: () {
                    if (_eventController.text.isEmpty) return;
                    if (_events[_controller.selectedDay] != null) {
                      _events[_controller.selectedDay].add(
                          prioritiesSelected.text +
                              " Priority" +
                              "\n" +
                              _startController.text +
                              "-" +
                              _endController.text +
                              "\n" +
                              _eventController.text);
                    } else {
                      _events[_controller.selectedDay] = [
                        prioritiesSelected.text +
                            " Priority" +
                            "\n" +
                            _startController.text +
                            "-" +
                            _endController.text +
                            "\n" +
                            _eventController.text
                      ];
                    }
                    prefs.setString("events", json.encode(encodeMap(_events)));
                    _eventController.clear();
                    _startController.clear();
                    _endController.clear();
                    prioritiesSelected.clear();
                    saveLog();
                    Navigator.pop(context);
                  },
                )
              ],
            ));
    setState(() {
      _events[_controller.selectedDay]
          .sort((a, b) => a.toString().compareTo(b.toString()));
      _selectedEvents = _events[_controller.selectedDay];
    });
  }
}
