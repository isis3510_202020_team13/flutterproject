import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ScheduleCard extends StatelessWidget{

  String subject = "Differential Equations";
  String hour = "7:50 - 11:00";
  String priority = "Low";
  String description = "Task 2";
  Color colour = new Color.fromRGBO(246,236,251,1.0);

  ScheduleCard(this.subject, this.hour, this.priority, this.description, this.colour);




  @override
  Widget build(BuildContext context) {


    double width = MediaQuery.of(context).size.width;


    final card = Container(
      height: 150.0,
      width: width,
      margin: EdgeInsets.only(
          top: 30.0,
          left: 20.0,
          right: 20.0
      ),
      decoration: BoxDecoration(
          color: colour,
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )
          ]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: 30,
                top: 15.0
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                    child: Text(
                      subject,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black
                      ),
                    ),
                    height: 30
                ),
                SizedBox(
                    height: 20,
                    child: Text(
                      hour,
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 12.0
                      ),
                    )
                ),
                SizedBox(
                  height: 20,
                  child: Text(
                    "Priority:" + priority,
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: 12.0
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                  child: Text(
                    "Description:" + description,
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: 12.0
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                  child: Text(
                    "Edit",
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 12.0
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),

    );

    // TODO: implement build
    return card;
  }

}