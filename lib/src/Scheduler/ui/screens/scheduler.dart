import 'package:f412/src/Scheduler/ui/widgets/schedule_card_list.dart';
import 'package:f412/src/Scheduler/ui/screens/scheduler_form_screen_state.dart';
import 'package:f412/src/Scheduler/ui/screens/sheduler_form_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Scheduler extends StatelessWidget{




  Widget build(BuildContext context) {

    void _addEvent(context){
      showModalBottomSheet(context: context, builder: (BuildContext bc){
        return SchedulerFormScreen();
      });
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[

        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blueAccent
        ),
        title: Text(
            "Scheduler",
          style: TextStyle(
            color: Colors.blueAccent
          ),
        ),
      ),
      body: ScheduleCardList(),
    floatingActionButton: FloatingActionButton(
    onPressed: () {
      _addEvent(context);
    // Add your onPressed code here!
    },
    backgroundColor: Colors.blueAccent,
      child: Icon(FontAwesomeIcons.plus),
    ));

  }

}



