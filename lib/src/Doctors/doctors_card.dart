import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class DoctorsCard extends StatelessWidget{
  @override

  String doctorsName;
  String speciality;
  double calification;
  String genre;
  String pathImage;
  String email;
  String phoneNumber;

  DoctorsCard(this.doctorsName, this.speciality, this.calification, this.genre, this.pathImage, this.email, this.phoneNumber);
  Widget build(BuildContext context) {

    void _createEmail() async{
      var emailaddress = 'mailto:'+this.email+'?subject=Sample Subject&body=This is a Sample email';

      if(await canLaunch(emailaddress)) {
        await launch(emailaddress);
      } else {
        throw 'Could not Email';
      }
    }

    double width = MediaQuery.of(context).size.width;

    final card = Container(
      height: 100.0,
      margin: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        top: 10.0,
        bottom: 10.0
      ),
      width: width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        shape: BoxShape.rectangle,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black38,
            blurRadius: 15.0,
            offset: Offset(0.0, 7.0)
          )
        ]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.only(
                left: 10.0,
                right: 10.0,
                top: 10.0,
                bottom: 10.0
              ),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(pathImage)
                )
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    top: 0.0
                  ),
                  child: Text(
                    doctorsName,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Text(
                  speciality,
                  style: TextStyle(
                    fontSize: 12.0
                  ),
                ),
                Row(
                  children: [
                    Icon(FontAwesomeIcons.solidStar, size: 15.0, color: Colors.yellow,),
                    Container(
                      margin: EdgeInsets.only(
                        left: 10.0,
                      ),
                      child: Text(
                        calification.toString(),
                        style: TextStyle(
                          fontSize: 15.0
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Row(
            children: [
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                    top: 10.0,
                  ),
                    child: Icon(FontAwesomeIcons.envelope, color: Colors.black38,)
                ),
                onTap: () {_createEmail();},
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                    top: 10.0,
                    right: 10.0
                  ),
                    child: Icon(FontAwesomeIcons.phoneSquareAlt, color: Colors.greenAccent,)
                ),
                onTap: () async => {await launch("tel:+57" + this.phoneNumber)},
              )
            ],
          )
        ],
      ),

    );

    // TODO: implement build
    return card;
  }

}