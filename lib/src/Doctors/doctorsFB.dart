import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:f412/src/Doctors/doctors_card.dart';
import 'package:f412/src/Hospitals/hospital_card.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class DoctorsFB extends StatefulWidget {
  @override
  _DoctorsFBState createState() => _DoctorsFBState();
}

class _DoctorsFBState extends State<DoctorsFB> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        title: Text(
          "Doctors",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: DoctorsListPage(),
    );
  }
}

class DoctorsListPage extends StatefulWidget {
  @override
  _DoctorsListPageState createState() => _DoctorsListPageState();
}

class _DoctorsListPageState extends State<DoctorsListPage> {
  Future getPosts() async {
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot qn = await firestore.collection("Doctors").get();

    return qn.docs;
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference doctors =
        FirebaseFirestore.instance.collection("Doctors");
    FirebaseStorage storage = FirebaseStorage.instance;
    return StreamBuilder<QuerySnapshot>(
      stream: doctors.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text("Something went wrong"),
          );
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: Text("Loading"),
          );
        }
        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            var doctorsName = document.data()['name'];
            var speciality = document.data()['speciality'];
            var rating = document.data()['rating'].toDouble();
            var genre = document.data()['genre'];
            var pathImage = document.data()['img'];
            var email = document.data()['email'];
            var phone = document.data()['phone'];
            return new DoctorsCard(doctorsName, speciality, rating, genre,
                pathImage, email, phone);
          }).toList(),
        );
      },
    );
  }
}
