import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:f412/src/Entries/entryEventAlarm.dart';
import 'package:f412/src/Entries/entryEventSchedule.dart';

class FirestoreService {
  FirebaseFirestore _db = FirebaseFirestore.instance;

  //Get Entries

  Stream<List<EntrySchedule>> getEntriesSchedule() {
    return _db.collection('schedule_events').snapshots().map((snapshot) =>
        snapshot.docs
            .map((doc) => EntrySchedule.fromJson(doc.data()))
            .toList());
  }

  Stream<List<EntryAlarm>> getEntriesAlarm() {
    return _db.collection('alarm_events').snapshots().map((snapshot) =>
        snapshot.docs.map((doc) => EntryAlarm.fromJson(doc.data())).toList());
  }

  //Upsert
  Future<void> setEntrySchedule(EntrySchedule entry) {
    var options = SetOptions(merge: true);

    return _db
        .collection('sheduler_events')
        .doc(entry.entryId)
        .set(entry.toMap(), options);
  }

  Future<void> setEntryAlarm(EntryAlarm entry) {
    var options = SetOptions(merge: true);

    return _db
        .collection('alarm_events')
        .doc(entry.entryId)
        .set(entry.toMap(), options);
  }

  //Delete
  Future<void> removeEntrySchedule(String entryId) {
    return _db.collection('sheduler_events').doc(entryId).delete();
  }

  Future<void> removeEntryAlarm(String entryId) {
    return _db.collection('alarm_events').doc(entryId).delete();
  }
}
