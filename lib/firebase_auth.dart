import 'package:connectivity/connectivity.dart';
import 'package:f412/register.dart';
import 'package:f412/signin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FirebaseAuthF412 extends StatefulWidget {
  @override
  _FirebaseAuthF412State createState() => _FirebaseAuthF412State();
}

class _FirebaseAuthF412State extends State<FirebaseAuthF412> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    // ignore: unrelated_type_equality_checks
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Login",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(16.0),
                child: Image(
                  width: width,
                  image: AssetImage("assets/img/logoF412.png"),
                ),
              ),
              Container(
                padding: EdgeInsets.all(16),
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 200,
                  child: RaisedButton(
                    color: Colors.white,
                    onPressed: () {
                      _push(context, SignIn());
                    },
                    child: Text(
                      "Sign-In",
                      style: TextStyle(color: Colors.lightBlue),
                    ),
                    padding: EdgeInsets.all(16),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(16),
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 200,
                  child: RaisedButton(
                    color: Colors.white,
                    onPressed: () {
                      _push(context, Register());
                    },
                    child: Text(
                      "Register",
                      style: TextStyle(color: Colors.lightBlue),
                    ),
                    padding: EdgeInsets.all(16),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _push(BuildContext context, Widget page) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return page;
    }));
  }
}
