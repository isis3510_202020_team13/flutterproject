import 'package:connectivity/connectivity.dart';
import 'package:f412/f412_class.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.lightBlue),
        title: Text(
          "Sign-In",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: withEmailPassword(),
    );
  }

  Widget withEmailPassword() {
    double width = MediaQuery.of(context).size.width;
    return Center(
      child: Form(
        key: _formKey,
        child: Container(
          margin: EdgeInsets.all(20.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.all(16.0),
                      child: Image(
                        width: width,
                        image: AssetImage("assets/img/logoF412.png"),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          labelText: "Email",
                          border: OutlineInputBorder(),
                        ),
                        validator: (String val) {
                          if (val.isEmpty) {
                            return "Please enter your Email";
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: TextFormField(
                        obscureText: true,
                        controller: _passwordController,
                        decoration: InputDecoration(
                          labelText: "Password",
                          border: OutlineInputBorder(),
                        ),
                        validator: (String val) {
                          if (val.isEmpty) {
                            return "Please enter your Password";
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      padding: const EdgeInsets.only(top: 16.0),
                      alignment: Alignment.center,
                      child: RaisedButton(
                        color: Colors.white,
                        child: Text(
                          "Sign In",
                          style: TextStyle(color: Colors.lightBlue),
                        ),
                        onPressed: () async {
                          _validator();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _validator() {
    if (_formKey.currentState.validate()) {
      _signinWithEmailPassword();
    }
  }

  void _signinWithEmailPassword() async {
    try {
      final User user = (await _auth.signInWithEmailAndPassword(
        email: _emailController.text,
        password: _passwordController.text,
      ))
          .user;
      if (!user.emailVerified) {
        await user.sendEmailVerification();
      }
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('email', user.email);
      prefs.setString('name', user.displayName);

      Navigator.of(context).push(MaterialPageRoute(builder: (_) {
        return F412Class();
      }));
    } catch (e) {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("No Internet connection please connect to continue"),
        ));
      }
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("Wrong email or password")));
      print(e);
    }
  }
}
