import 'package:connectivity/connectivity.dart';
import 'package:f412/logout.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'f412_class.dart';
import 'firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _displayName = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isSucces;
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double hegiht = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.lightBlue),
        title: Text(
          "Register",
          style: TextStyle(
            color: Colors.lightBlue,
          ),
        ),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.all(16.0),
                        child: Image(
                          image: AssetImage("assets/img/logoF412.png"),
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        child: TextFormField(
                          controller: _displayName,
                          decoration: const InputDecoration(
                            labelText: "Full Name",
                            border: OutlineInputBorder(),
                          ),
                          validator: (String val) {
                            if (val.isEmpty) {
                              return "Please enter your full name";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        child: TextFormField(
                          controller: _emailController,
                          decoration: const InputDecoration(
                            labelText: "Email",
                            border: OutlineInputBorder(),
                          ),
                          validator: (String val) {
                            if (val.isEmpty) {
                              return "Please enter your email";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                        child: TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          decoration: const InputDecoration(
                            labelText: "Password",
                            border: OutlineInputBorder(),
                          ),
                          validator: (String val) {
                            if (val.isEmpty) {
                              return "Please enter your full name";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: RaisedButton(
                          color: Colors.white,
                          onPressed: () async {
                            _register();
                          },
                          child: Text(
                            "Register",
                            style: TextStyle(color: Colors.lightBlue),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _register() {
    if (_formKey.currentState.validate()) {
      _registerAcount();
    }
  }

  void _registerAcount() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        Widget okButton = FlatButton(
          child: Text("OK"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        );
        return Scaffold(
          backgroundColor: Colors.white,
          body: AlertDialog(
            content: Text("No Internet connection please connect to continue"),
            actions: [okButton],
          ),
        );
      }));
    }
    final User user = (await _auth.createUserWithEmailAndPassword(
            email: _emailController.text, password: _passwordController.text))
        .user;
    if (user != null) {
      if (!user.emailVerified) {
        await user.sendEmailVerification();
      }
      await user.updateProfile(displayName: _displayName.text);
      final user1 = _auth.currentUser;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('email', user1.email);
      prefs.setString('name', user.displayName);

      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return F412Class();
      }));
    } else {
      _isSucces = false;
    }
  }
}
