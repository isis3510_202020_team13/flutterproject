import 'package:f412/firebase_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogOut extends StatefulWidget {
  final User user;

  const LogOut({Key key, this.user}) : super(key: key);
  @override
  _LogOutState createState() => _LogOutState();
}

class _LogOutState extends State<LogOut> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: <Widget>[],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.redAccent),
        title: Text(
          "Log-Out",
          style: TextStyle(
            color: Colors.redAccent,
          ),
        ),
      ),
      body: Center(
        child: Container(
          width: width,
          margin: EdgeInsets.all(20.0),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Flexible(
                    child: Image(
                      image: AssetImage("assets/img/logoF412.png"),
                    ),
                  ),
                  Flexible(
                    child: RaisedButton(
                      color: Colors.white,
                      child: Text(
                        "Log-Out",
                        style: TextStyle(color: Colors.redAccent),
                      ),
                      onPressed: () async {
                        _logOut();
                      },
                    ),
                  ),
                  Flexible(
                    child: RaisedButton(
                      color: Colors.white,
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.lightBlue),
                      ),
                      onPressed: () async {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('email');
    _signOut().whenComplete(() {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => FirebaseAuthF412()));
    });
  }

  Future _signOut() async {
    await _auth.signOut();
  }
}
